
*******
General
*******

*Sculpt Mode* is similar to *Edit Mode* in that it is used to alter the shape of a model,
but Sculpt Mode uses a very different workflow:
instead of dealing with individual elements (vertices, edges, and faces),
an area of the model is primarily changed using brushes.

.. figure:: /images/sculpt-paint_sculpting_introduction_example.png

   Sculpting Mode Example.

Sculpt Mode is accessed from the mode menu of the :doc:`3D Viewport header </editors/3dview/modes>`
or with the pie menu via :kbd:`Ctrl-Tab`.
Once inside Sculpt Mode, the Toolbar and Tool Settings of the 3D Viewport will change to
Sculpt Mode specific panels. The cursor will change to a circle, to indicate the size of the brush.

.. warning::

   To have predictable brush behavior,
   make sure to :doc:`apply the scale </scene_layout/object/editing/apply>` of your mesh.

The following pages will briefly explain the fundamental features and concepts of *Sculpt Mode*,
including various links to other pages for more details.