
#############
  Baking
#############

These add-ons add UI elements for faster baking access.

.. toctree::
   :maxdepth: 1

   vdm_brush_baker.rst
